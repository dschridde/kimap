/*
    SPDX-FileCopyrightText: 2009 Andras Mantia <amantia@kde.org>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#ifndef KIMAP_EXPUNGEJOB_H
#define KIMAP_EXPUNGEJOB_H

#include "kimap_export.h"

#include "job.h"

namespace KIMAP
{

class Session;
struct Response;
class ExpungeJobPrivate;

/**
 * Expunges the deleted messages in the selected mailbox.
 *
 * This permanently removes any messages that have the
 * \Deleted flag set in the selected mailbox.
 *
 * This job can only be run when the session is in the
 * selected state.
 *
 * If the server supports ACLs, the user will need the
 * Acl::Expunge right on the mailbox.
 */
class KIMAP_EXPORT ExpungeJob : public Job
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(ExpungeJob)

    friend class SessionPrivate;

public:
    explicit ExpungeJob(Session *session);
    ~ExpungeJob() override;

protected:
    void doStart() override;
    void handleResponse(const Response &response) override;
};

}

#endif
